﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using LabWorkFooddDelivery.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;

namespace LabWorkFooddDelivery.Areas.Identity.Pages.Account
{
  [AllowAnonymous]
  public class RegisterModel : PageModel
  {
    private readonly SignInManager<ApplicationUser> _signInManager;
    private readonly UserManager<ApplicationUser> _userManager;
    private readonly ILogger<RegisterModel> _logger;
    private readonly IEmailSender _emailSender;

    public RegisterModel(
      UserManager<ApplicationUser> userManager,
      SignInManager<ApplicationUser> signInManager,
      ILogger<RegisterModel> logger)
    {
      _userManager = userManager;
      _signInManager = signInManager;
      _logger = logger;
    }

    [BindProperty] public InputModel Input { get; set; }

    public string ReturnUrl { get; set; }

    public class InputModel
    {
      [Required(ErrorMessage = "Укажите Email")]
      [EmailAddress]
      [Display(Name = "Email")]
      public string Email { get; set; }


      [Required(ErrorMessage = "Укажите возраст")]
      [Display(Name = "Возраст")]
      public int Age { get; set; }


      [Required(ErrorMessage = "Укажите Имя")]
      [Display(Name = "Имя")]
      public string Name { get; set; }

      [Required]
      [StringLength(100, ErrorMessage = "{0} должен быть не менее {2}, а при максимальных {1} символах.",
        MinimumLength = 6)]
      [DataType(DataType.Password)]
      [Display(Name = "Пароль")]
      public string Password { get; set; }

      [DataType(DataType.Password)]
      [Display(Name = "Подтвердите пароль")]
      [Compare("Password", ErrorMessage = "Пароль и пароль подтверждения не совпадают.")]
      public string ConfirmPassword { get; set; }
    }

    public void OnGet(string returnUrl = null)
    {
      ReturnUrl = returnUrl;
    }

    public async Task<IActionResult> OnPostAsync(string returnUrl = null)
    {
      returnUrl = returnUrl ?? Url.Content("~/");
      if (ModelState.IsValid)
      {
        var user = new ApplicationUser {UserName = Input.Email, Name = Input.Name,Age = Input.Age,Email = Input.Email, };
        var result = await _userManager.CreateAsync(user, Input.Password);
        if (result.Succeeded)
        {
          _logger.LogInformation("User created a new account with password.");
          
          await _signInManager.SignInAsync(user, isPersistent: false);
          return LocalRedirect(returnUrl);
        }

        foreach (var error in result.Errors)
        {
          ModelState.AddModelError(string.Empty, error.Description);
        }
      }

      // If we got this far, something failed, redisplay form
      return Page();
    }
  }
}