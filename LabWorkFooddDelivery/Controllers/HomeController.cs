﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using LabWorkFooddDelivery.Areas.Identity.Services;
using LabWorkFooddDelivery.Data;
using Microsoft.AspNetCore.Mvc;
using LabWorkFooddDelivery.Models;
using LabWorkFooddDelivery.Models.Entity;
using LabWorkFooddDelivery.ViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;

namespace LabWorkFooddDelivery.Controllers
{
  public class HomeController : Controller
  {
    private readonly ApplicationDbContext _context;
    private readonly IHostingEnvironment _environment;
    private FileUploadService _fileUploadService;

    public HomeController(ApplicationDbContext context, IHostingEnvironment environment, FileUploadService fileUploadService)
    {
      _context = context;
      _environment = environment;
      _fileUploadService = fileUploadService;
    }

    public IActionResult About()
    {
      ViewData["Message"] = "Your application description page.";

      return View();
    }

    public IActionResult Contact()
    {
      ViewData["Message"] = "Your contact page.";

      return View();
    }

    public IActionResult Privacy()
    {
      return View();
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
      return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }


   

    // GET: Enterprises
    public async Task<IActionResult> Index()
    {
      return View(await _context.Enterprises.ToListAsync());
    }

    // GET: Enterprises/Details/5
    public async Task<IActionResult> Details(string id)
    {
      if (id == null)
      {
        return NotFound();
      }
      

      Enterprise enterprise = await _context.Enterprises.Include(c=>c.Foods)
          .FirstOrDefaultAsync(m => m.Id == id);
      
      if (enterprise == null)
      {
        return NotFound();
      }


      return View(enterprise);
    }

    // GET: Enterprises/Create
    public IActionResult Create()
    {
      return View();
    }

    // POST: Enterprises/Create
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
    // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Create(CreateEnterpriseViewModel model)
    {
      if (ModelState.IsValid)
      {
        var path = Path.Combine(_environment.WebRootPath, $"images\\{model.Name}\\avatar");
        _fileUploadService.Upload(path, model.ImagePath.FileName, model.ImagePath);

        Enterprise enterprise = new Enterprise()
        {
        Name = model.Name,
          Description = model.Description
        }; 
        
        enterprise.ImagePath = $"images/{model.Name}/avatar/{model.ImagePath.FileName}";
        _context.Add(enterprise);
        await _context.SaveChangesAsync();
        return RedirectToAction(nameof(Index));
      }
      return View(model);
    }


    // GET: Foods/Create
    public IActionResult AddSellBox(String Id)
    {
      ViewData["EnterpriseId"] = _context.Enterprises.FirstOrDefault(x => x.Id.Equals(Id));
      return RedirectToAction(nameof(Details));
    }  
    
    // GET: Foods/Create
    public IActionResult CreateFood(String Id)
    {
      Enterprise enterprise = _context.Enterprises.FirstOrDefault(x => x.Id.Equals(Id));
      if (enterprise == null)
      {
        return NotFound("Нет предприятия к кому возможно добавить новое блюдо");
      }
      ViewData["EnterpriseId"] = enterprise.Id;
      return View();
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> CreateFood(CreateFoodViewModel model)
    {
      if (ModelState.IsValid)
      {

        Enterprise enterprise = _context.Enterprises.FirstOrDefault(x => x.Id.Equals(model.EnterpriseId));
        if (enterprise != null)
        {

          Food food = new Food()
          {
            Enterprise = enterprise,
            Name = model.Name,
            Description = model.Description,
            Price = model.Price,
            EnterpriseId = model.EnterpriseId
          };

          var path = Path.Combine(_environment.WebRootPath,
            $"images\\{enterprise.Name}\\foods\\{model.Name}");
          _fileUploadService.Upload(path, model.ImagePath.FileName, model.ImagePath);
          food.ImagePath = $"images/{enterprise.Name}/foods/{model.Name}/{model.ImagePath.FileName}";
          _context.Add(food);
          await _context.SaveChangesAsync();
          return RedirectToAction(nameof(Index));

        }
        
      }
      ViewData["EnterpriseId"] = _context.Enterprises.FirstOrDefault(x => x.Id.Equals(model.EnterpriseId));
      return View(model);
    }
    
  }
}
