﻿using System;
using System.Collections.Generic;
using System.Text;
using LabWorkFooddDelivery.Data.Configurations;
using LabWorkFooddDelivery.Models;
using LabWorkFooddDelivery.Models.Entity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace LabWorkFooddDelivery.Data
{
  public class ApplicationDbContext : IdentityDbContext
  {
    public DbSet<ApplicationUser> Users { get; set; }
    public DbSet<Food> Foods{ get; set; }
    public DbSet<Order> Orders { get; set; }
    public DbSet<Enterprise> Enterprises { get; set; }

    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
        : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder builder)
    {
      base.OnModelCreating(builder);
      builder.ApplyConfiguration(new FoodConfiguration());
      builder.ApplyConfiguration(new OrderConfiguration());
    }
  }
}
