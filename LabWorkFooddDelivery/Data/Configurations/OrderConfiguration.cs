﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LabWorkFooddDelivery.Models.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LabWorkFooddDelivery.Data.Configurations
{
  public class OrderConfiguration : IEntityTypeConfiguration<Order>
  {
    public void Configure(EntityTypeBuilder<Order> builder)
    {
      builder.HasOne(c => c.User)
        .WithMany(c => c.Orders)
        .HasForeignKey(c => c.UserId)
        .OnDelete(DeleteBehavior.Restrict);
    }
  }
}
