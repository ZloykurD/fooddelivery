﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LabWorkFooddDelivery.Models.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LabWorkFooddDelivery.Data.Configurations
{
  public class FoodConfiguration : IEntityTypeConfiguration<Food>
  {
    public void Configure(EntityTypeBuilder<Food> builder)
    {
      builder.HasOne(c => c.Enterprise)
        .WithMany(c => c.Foods)
        .HasForeignKey(c => c.EnterpriseId)
        .OnDelete(DeleteBehavior.Restrict);
    }
  }
}