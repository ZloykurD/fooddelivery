﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LabWorkFooddDelivery.Data.Migrations
{
    public partial class v2_AddFieldFoodImagePath : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ImagePath",
                table: "Foods",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ImagePath",
                table: "Foods");
        }
    }
}
