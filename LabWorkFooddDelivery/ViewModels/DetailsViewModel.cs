﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using LabWorkFooddDelivery.Models.Entity;
using Microsoft.AspNetCore.Http;

namespace LabWorkFooddDelivery.ViewModels
{
  public class DetailsViewModel
  {

    public String  Id { get; set; }

    [Required]
    [Display(Name = "Название")]
    public String Name { get; set; }
    [Required]
    [Display(Name = "Фото")]
    public String ImagePath { get; set; }
    [Required]
    [Display(Name = "Описание")]
    public String Description { get; set; }

    public IEnumerable<Food> Foods { get; set; }

  }
}
