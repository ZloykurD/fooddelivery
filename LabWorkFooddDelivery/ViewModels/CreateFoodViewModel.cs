﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace LabWorkFooddDelivery.ViewModels
{
  public class CreateFoodViewModel
  {
    [Required]
    [Display(Name = "Название")]
    public String Name { get; set; }

    [Required]
    [Display(Name = "Стоимость")]
    public double Price { get; set; }

    [Required] [Display(Name = "Фото")] 
    public IFormFile ImagePath { get; set; }

    [Required]
    [Display(Name = "Описание")]
    public String Description { get; set; }

    public String EnterpriseId { get; set; }
  }
}