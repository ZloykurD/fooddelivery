﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LabWorkFooddDelivery.Models.Entity
{
  public class Food
  {
    public String Id { get; set; }
    public String Name { get; set; }
    public String Description { get; set; }
    public double Price { get; set; }
    public String EnterpriseId { get; set; }
    public String ImagePath { get; set; }
    public Enterprise Enterprise { get; set; }
  }
}