﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LabWorkFooddDelivery.Models.Entity
{
  public class Enterprise
  {
    public String Id { get; set; }
    public String Name { get; set; }
    public String ImagePath { get; set; }
    public String Description { get; set; }

    public IEnumerable<Food> Foods { get; set; }
  }
}
