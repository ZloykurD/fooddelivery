﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LabWorkFooddDelivery.Models.Entity;
using Microsoft.AspNetCore.Identity;

namespace LabWorkFooddDelivery.Models
{
  public class ApplicationUser :IdentityUser
  {
    public int Age { get; set; }
    public String Name { get; set; }


    public IEnumerable<Order> Orders { get; set; }
  }
}
