﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LabWorkFooddDelivery.Models.Entity
{
  public class Order
  {
    public String Id { get; set; }
    public String Name { get; set; }
    public DateTime DateTime { get; set; }
    public int Quantity { get; set; }
    public double Total { get; set; }
    public String UserId { get; set; }
    public ApplicationUser User { get; set; }
  }
}
